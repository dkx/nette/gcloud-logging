<?php

declare(strict_types=1);

namespace DKX\NetteGCloudLogging;

use Google\Cloud\Logging\LoggingClient;

interface LoggingClientFactory
{
	public function create() : LoggingClient;
}
