<?php

declare(strict_types=1);

namespace DKX\NetteGCloudLogging;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloud\ProjectId\ProjectIdProvider;
use Google\Cloud\Logging\LoggingClient;

final class LoggingClientFactoryImpl implements LoggingClientFactory
{
	private ProjectIdProvider $projectIdProvider;

	private CredentialsProvider $credentialsProvider;

	public function __construct(ProjectIdProvider $projectIdProvider, CredentialsProvider $credentialsProvider)
	{
		$this->projectIdProvider   = $projectIdProvider;
		$this->credentialsProvider = $credentialsProvider;
	}

	/**
	 * @codeCoverageIgnore
	 */
	public function create() : LoggingClient
	{
		return new LoggingClient([
			'projectId' => $this->projectIdProvider->getProjectId(),
			'keyFile' => $this->credentialsProvider->getCredentials(),
		]);
	}
}
