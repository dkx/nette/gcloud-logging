<?php

declare(strict_types=1);

namespace DKX\NetteGCloudLogging\DI;

use DKX\NetteGCloudLogging\LoggingClientFactory;
use DKX\NetteGCloudLogging\LoggingClientFactoryImpl;
use Google\Cloud\Logging\LoggingClient;
use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Statement;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use function assert;
use function is_object;
use function is_string;

/**
 * @codeCoverageIgnore
 */
final class GCloudLoggingExtension extends CompilerExtension
{
	public function getConfigSchema() : Schema
	{
		return Expect::structure([
			'factory' => Expect::anyOf(Expect::string(), Expect::type(Statement::class))
				->default(LoggingClientFactoryImpl::class),
		]);
	}

	public function loadConfiguration() : void
	{
		$builder = $this->getContainerBuilder();
		$config  = $this->getConfig();
		assert(is_object($config));

		$factory = $builder
			->addDefinition($this->prefix('logging.factory'))
			->setType(LoggingClientFactory::class)
			->setAutowired(false);

		if (is_string($config->factory)) {
			$factory->setFactory($config->factory);
		} elseif ($config->factory instanceof Statement) {
			assert(is_string($config->factory->entity));
			$factory->setFactory($config->factory->entity, $config->factory->arguments);
		}

		$builder
			->addDefinition($this->prefix('logging.client'))
			->setType(LoggingClient::class)
			->setFactory([$factory, 'create']);
	}
}
