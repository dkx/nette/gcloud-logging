# DKX/NetteGCloudLogging

Google cloud logging integration for Nette DI

## Installation

```bash
$ composer require dkx/nette-gcloud
$ composer require dkx/nette-gcloud-logging
```

## Usage

```yaml
extensions:
    gcloud: DKX\NetteGCloud\DI\GCloudExtension
    gcloud.logging: DKX\NetteGCloudLogging\DI\GCloudLoggingExtension
```

Now you'll be able to simply inject the `Google\Cloud\Logging\LoggingClient`.
