<?php

declare(strict_types=1);

namespace DKXTests\NetteGCloudLogging\Tests;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloud\ProjectId\ProjectIdProvider;
use DKX\NetteGCloudLogging\LoggingClientFactoryImpl;
use DKXTests\NetteGCloudLogging\TestCase;
use Mockery;
use function assert;

final class LoggingClientFactoryImplTest extends TestCase
{
	public function testCreate() : void
	{
		$projectIdProvider = Mockery::mock(ProjectIdProvider::class)
			->shouldReceive('getProjectId')->andReturn('abcd')->getMock();
		assert($projectIdProvider instanceof ProjectIdProvider);

		$credentialsProvider = Mockery::mock(CredentialsProvider::class)
			->shouldReceive('getCredentials')->andReturn([])->getMock();
		assert($credentialsProvider instanceof CredentialsProvider);

		$factory = new LoggingClientFactoryImpl($projectIdProvider, $credentialsProvider);
		$factory->create();
	}
}
